import io

from database.page.schema import SchemaPage
from database.page.free import FreePage
from database.page.data import (
    Cell,
    Slot,
    DataPage
)

slot = Slot()

cell = Cell()
cell.name = "hi"
cell.value = "world"

slot.cells.append(cell)

cell = Cell()
cell.name = "hola"
cell.value = 5

slot.cells.append(cell)

page = DataPage()
page.page_id = 0
page.slots.append(slot)

f = open("test.db", "wb")
f.write(page.serialize())
f.close()

f = open("test.db", "rb")
f.read(5)

s = io.BytesIO(f.read())
page = DataPage.deserialize(0, s)

print(page.slots[0].cells[0].name)
print(page.slots[0].cells[0].value)

print(page.slots[0].cells[1].name)
print(page.slots[0].cells[1].value)

