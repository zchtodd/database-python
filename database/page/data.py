import io

from enum import IntEnum
from struct import unpack, pack
from database.page import Page

class ValueTypes(IntEnum):
    STRING = 0
    INTEGER = 1
    FLOAT = 2

class Cell(object):

    @classmethod
    def deserialize(cls, s):
        cell = Cell()

        namelen = unpack("<i", s.read(4))[0]
        name = unpack("<{}s".format(namelen), s.read(namelen))[0]
        cell.name = name.decode("utf-8")

        valuetype = unpack("<b", s.read(1))[0]

        if valuetype == ValueTypes.STRING:
            valuelen = unpack("<i", s.read(4))[0]
            value = unpack("<{}s".format(valuelen), s.read(valuelen))[0]
            cell.value = value.decode("utf-8")
        elif valuetype == ValueTypes.INTEGER:
            unpack("<i", s.read(4))[0]
            cell.value = unpack("<i", s.read(4))[0]
        elif valuetype == ValueTypes.FLOAT:
            unpack("<i", s.read(4))[0]
            cell.value = unpack("<f", s.read(4))[0]

        return cell

    def serialize(self):
        name = self.name.encode("utf-8")

        data = io.BytesIO(b"")
        data.write(pack("<i", len(name)))
        data.write(pack("<{}s".format(len(name)), name))

        if isinstance(self.value, str):
            value = self.value.encode("utf-8")

            data.write(pack("<b", ValueTypes.STRING))
            data.write(pack("<i", len(value)))
            data.write(pack("<{}s".format(len(value)), value))
        elif isinstance(self.value, int):
            data.write(pack("<b", ValueTypes.INTEGER))
            data.write(pack("<i", 4))
            data.write(pack("<i", self.value))
        elif isinstance(self.value, float):
            data.write(pack("<b", ValueTypes.FLOAT))
            data.write(pack("<i", 4))
            data.write(pack("<f", self.value))

        data.seek(0)
        return data.read()


class Slot(object):
    def __init__(self):
        self.slot_id = 0
        self.offset = 0
        self.cells = []
        self.overflow_page_id = 0
        self.overflow_slot_id = 0

    @classmethod
    def deserialize(cls, slot_id, s):
        slot = cls()
        slot.slot_id = slot_id

        slot.offset = unpack("<i", s.read(4))[0]
        slot.length = unpack("<i", s.read(4))[0]
        cell_count = unpack("<i", s.read(4))[0]
        slot.overflow_page_id = unpack("<i", s.read(4))[0]
        slot.overflow_slot_id = unpack("<i", s.read(4))[0]
        
        i = 0
        while i < cell_count:
            slot.cells.append(Cell.deserialize(s))
            i += 1
        return slot

    def serialize(self):
        slot_data = io.BytesIO(b"")
        cell_data = io.BytesIO(b"")
        for cell in self.cells:
            cell_data.write(cell.serialize())

        length = 24 + cell_data.tell()
        slot_data.write(pack("<6i", self.slot_id, self.offset, length, 
            len(self.cells), self.overflow_page_id, self.overflow_slot_id))

        slot_data.seek(0)
        cell_data.seek(0)
        return slot_data.read() + cell_data.read()


class DataPage(Page):

    PAGE_TYPE = 4

    def __init__(self):
        self.slots = []

    @classmethod
    def deserialize(cls, page_id, s):
        page = cls()
        page.page_id = page_id

        slot_count = unpack("<h", s.read(2))[0]

        i = 0
        while i < slot_count:
            slot_id = unpack("<i", s.read(4))[0]
            page.slots.append(Slot.deserialize(slot_id, s))
            i += 1
        return page

    def serialize(self):
        data = io.BytesIO(b"")
        data.write(pack("<i", self.page_id))
        data.write(pack("<b", DataPage.PAGE_TYPE))
        data.write(pack("<h", len(self.slots)))

        for slot in self.slots:
            data.write(slot.serialize())

        data.seek(0)
        return self.pad(data.read())

